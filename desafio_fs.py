# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Considere um modelo de informação, onde um registro é representado por uma "tupla".
# Uma tupla (ou lista) nesse contexto é chamado de fato.

# Exemplo de um fato:
# ('joão', 'idade', 18, True)

# Nessa representação, a entidade (E) 'joão' tem o atributo (A) 'idade' com o valor (V) '18'.

# Para indicar a remoção (ou retração) de uma informação, o quarto elemento da tupla pode ser 'False'
# para representar que a entidade não tem mais aquele valor associado aquele atributo.


# Como é comum em um modelo de entidades, os atributos de uma entidade pode ter cardinalidade 1 ou N (muitos).

# Segue um exemplo de fatos no formato de tuplas (i.e. E, A, V, added?)

facts = [
    ('gabriel', 'endereço', 'av rio branco, 109', True),
    ('joão', 'endereço', 'rua alice, 10', True),
    ('joão', 'endereço', 'rua bob, 88', True),
    ('joão', 'telefone', '234-5678', True),
    ('joão', 'telefone', '91234-5555', True),
    ('joão', 'telefone', '234-5678', False),
    ('gabriel', 'telefone', '98888-1111', True),
    ('gabriel', 'telefone', '56789-1010', True)
]


# Vamos assumir que essa lista de fatos está ordenada dos mais antigos para os mais recentes.


# Nesse schema,
# o atributo 'telefone' tem cardinalidade 'muitos' (one-to-many), e 'endereço' é 'one-to-one'.
schema = [
    ('endereço', 'cardinality', 'one'),
    ('telefone', 'cardinality', 'many')
]


# Nesse exemplo, os seguintes registros representam o histórico de endereços que joão já teve:
#  (
#   ('joão', 'endereço', 'rua alice, 10', True)
#   ('joão', 'endereço', 'rua bob, 88', True),
#  )
# E o fato considerado vigente (ou ativo) é o último.


# O objetivo desse desafio é escrever uma função que retorne quais são os fatos vigentes sobre essas entidades.
# Ou seja, quais são as informações que estão valendo no momento atual.
# A função deve receber `facts` (todos fatos conhecidos) e `schema` como argumentos.

# Função para imprimir de forma
def print_lista(lista):
    for item in lista:
        print(item)
    print()


def verificar_fatos(facts, schema):
    true_facts = []  # Lista dos fatos vigentes sobre as entidades presentes
    last_schema_item = schema[-1][0]
    # Ultimo atributo do schema
    # para verificar se o atributo nos fatos não está listado no schema

    for fact in facts:
        # Nomeando cada valor para facilitar entendimento do código
        entity = fact[0]
        attribute = fact[1]
        value = fact[2]

        if fact[-1]:  # verificando se o item foi removido ou não
            for item in schema:
                if attribute == item[0]:
                    if item[1] == 'cardinality':
                        if item[2] == 'one':
                            for t_fact in true_facts:
                                if (entity in t_fact) and (attribute in t_fact):
                                    true_facts.remove(t_fact)
                            true_facts.append(fact)

                        elif item[2] == 'many':
                            true_facts.append(fact)
                        else:
                            raise RuntimeError("cardinalidade não identificada ->", item[2])
                    else:
                        raise RuntimeError("item do schema referente a uma relação não reconhecida ->", item[1])
                    break
                elif attribute != item[0] and last_schema_item == item[0]:
                    raise RuntimeError("atributo não encontrado no schema ->", item[0], attribute)
        else:
            for t_fact in true_facts:
                if (entity in t_fact) and (attribute in t_fact) and (value in t_fact):
                    true_facts.remove(t_fact)

    print_lista(true_facts)


verificar_fatos(facts, schema)

# Entrada do exemplo:
# [
#   ('gabriel', 'endereço', 'av rio branco, 109', True),
#   ('joão', 'endereço', 'rua alice, 10', True),
#   ('joão', 'endereço', 'rua bob, 88', True),
#   ('joão', 'telefone', '234-5678', True),
#   ('joão', 'telefone', '91234-5555', True),
#   ('joão', 'telefone', '234-5678', False),
#   ('gabriel', 'telefone', '98888-1111', True),
#   ('gabriel', 'telefone', '56789-1010', True),
# ]
# Resultado esperado para este exemplo (mas não precisa ser nessa ordem):
# [
#   ('gabriel', 'endereço', 'av rio branco, 109', True),
#   ('joão', 'endereço', 'rua bob, 88', True),
#   ('joão', 'telefone', '91234-5555', True),
#   ('gabriel', 'telefone', '98888-1111', True),
#   ('gabriel', 'telefone', '56789-1010', True)
# ]
